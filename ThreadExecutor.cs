﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadQueueLock_hw
{
    public class ThreadExecutor
    {
        Queue<Thread> threads = new Queue<Thread>();
        public object key = new object();

        public void Add(Thread thread)
        {
            lock (key)
            {
                if (thread != null)
                {
                    threads.Enqueue(thread);
                }
            }
        }

        public void Execute()
        {
            lock (key)
            {
                while (threads.Count > 0)
                {
                    Thread current = threads.Dequeue();
                    current.Start();
                }
            }
        }

    }
}
